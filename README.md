# CICD files for Gitlab Pipelines

An initial push to convert the [CICD course workflow fileas](https://github.com/hamelsmu/wandb-cicd) to Gitlab Pipelines. Any help appreciated!

> The course Page: https://www.wandb.courses/courses/take/ci-cd-for-machine-learning

## `ci.yaml`

This GitLab CI/CD file contains some differences from the GitHub Actions workflow:

 1 The GitLab CI/CD script uses a .gitlab-ci.yml format instead of the GitHub Actions YAML format.
 2 The runs-on property has been replaced by the image property, which now uses the python:latest image, an environment similar to ubuntu-latest.
 3 The steps section has been replaced by the script section in the build job.
 4 Instead of on: [pull_request], GitLab CI has the only keyword with the value - merge_requests, which binds the trigger to merge requests.

 ## `tests.yaml`

This file has the following steps:

 1 Use the Python 3.8 Docker image.
 2 Define a single stage for running the tests.
 3 Set the WANDB_API_KEY variable.
 4 Update and install Mamba (the Conda alternative) using wget, initialize Conda, create and activate the environment, and then install pytest.
 5 Define the "unit_and_smoke_tests" job to run the tests with pytest only when there are changes to the test-example folder.
 6 Cache the 'course' folder to save time for subsequent pipeline runs.

 ## `secrets.yaml`

 Note that this GitLab pipeline file assumes that you have the MY_SECRET variable stored as a CI/CD variable in your GitLab project settings.


 ## `io.yaml`

 Please note that the functionality of "output variables" in GitHub Actions does not directly translate to GitLab CI/CD. Variables are usually declared as environment
variables and shared across different jobs in the same stage, you can use artifacts if you need to share variables across stages.
